# Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`class `[`Gendy`](#classGendy) | 
`class `[`ofApp`](#classofApp) | 

# class `Gendy` 

```
class Gendy
  : public ofThread
```  

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public ofstream `[`p_outfile`](#classGendy_1a7a7ec781ec4a79b6d3c9f29ebbe22e9b) | File stream (& descriptor) to store [Gendy](#classGendy) pcm values.
`public  `[`Gendy`](#classGendy_1a66cd4ff0bef65bd1f6a8d2e125915ff4)`()` | This is the [Gendy](#classGendy) algorithm as defined by Iannis Xenakis and implemented by Sergio Luque Breakpoints(x for time and y for amplitude) are generated using certain stochastic methods. Interpolate between the breakpoints to generate a new waveform, either for playback as audio or for control values
`public  `[`Gendy`](#classGendy_1a03d7d2e1cd128c870da4b64734e3f482)`(unsigned int numBreakPoints,unsigned int minFrequency,unsigned int maxFrequency,unsigned int bufferSize)` | Initialize the class with required defaults.
`public  `[`~Gendy`](#classGendy_1a4317e1fc53d407c165eb626cd48e3d89)`()` | 
`public void `[`setup`](#classGendy_1a9f8c0e7adfc17e0d7fa9844531f65e70)`(unsigned int numBreakPoints,unsigned int minFrequency,unsigned int maxFrequency,unsigned int bufferSize)` | Initialize the class with required defaults Either Initialize at construction or use an empty constructor and call `[Gendy::setup](#classGendy_1a9f8c0e7adfc17e0d7fa9844531f65e70)` at a later stage
`public double `[`drunkWalk`](#classGendy_1aaf5753120e1374ece4bb29352307d8b7)`()` | A simple implementation of Brownian motion. This call returns a single value. Use it in a `while` loop for continuous updating
`public double `[`poission`](#classGendy_1a537ab206639e1601ab1e9d0df3982198)`()` | A simple implementation of Poission distribution This call returns a single value. Use it in a `while` loop for continuous updates
`public void `[`process`](#classGendy_1a0b16221aaa81a6e4a3e109cd6fb0f67d)`()` | The implementation of [Gendy](#classGendy) is multithreaded. This call initiates the process.
`public void `[`threadedFunction`](#classGendy_1ad98694e34171a373dfbdd254c34f73c6)`()` | This is the threaded function that executes the [Gendy](#classGendy) algorithm. This generates an array of breakpoints using the desired random distribution function. Since it is a threaded function, while this process is ongoing, the array cannot be accessed by other functions.
`public double `[`getGendySample`](#classGendy_1a42580a4bbb474612012585f06375b633)`()` | Calculates and returns a single sample from the [Gendy](#classGendy) waveform buffer. It updates the counter for each call to use as an index inside the array. When the counter reaches the end of the array, it restarts `[Gendy::process](#classGendy_1a0b16221aaa81a6e4a3e109cd6fb0f67d)` to calculate new breakpoints and repeats its internal functions.
`public void `[`setFrequencies`](#classGendy_1a8230d0b38c7a12e4dae31745a577d7e1)`(float minFreq,float maxFreq)` | Set the bounds of minimum and maximum frequency values for breakpoint bounds The normalized values `minFreq/numBreakpoints` set the new bounds. This restarts the [Gendy](#classGendy) process using `[Gendy::process](#classGendy_1a0b16221aaa81a6e4a3e109cd6fb0f67d)`
`public void `[`setNewBreakpoints`](#classGendy_1a6656e6af1bfe340f67ad74fdf8b99a8b)`(int maxLength,int numBreakPoints)` | Set new values for number of breakpoints and max size of the array This restarts the [Gendy](#classGendy) process using `[Gendy::process](#classGendy_1a0b16221aaa81a6e4a3e109cd6fb0f67d)`
`public void `[`plotGendy`](#classGendy_1afdb82a587cbc9a9f77a106be9e12f958)`()` | Plot the [Gendy](#classGendy) waveform using `gnuplot`
`public inline double `[`getGendyAt`](#classGendy_1afa7001a6d8663361cf7192e3fc0ad406)`(unsigned int position)` | Returns a single [Gendy](#classGendy) waveform sample at the requested index.

## Members

#### `public ofstream `[`p_outfile`](#classGendy_1a7a7ec781ec4a79b6d3c9f29ebbe22e9b) 

File stream (& descriptor) to store [Gendy](#classGendy) pcm values.

#### `public  `[`Gendy`](#classGendy_1a66cd4ff0bef65bd1f6a8d2e125915ff4)`()` 

This is the [Gendy](#classGendy) algorithm as defined by Iannis Xenakis and implemented by Sergio Luque Breakpoints(x for time and y for amplitude) are generated using certain stochastic methods. Interpolate between the breakpoints to generate a new waveform, either for playback as audio or for control values

#### `public  `[`Gendy`](#classGendy_1a03d7d2e1cd128c870da4b64734e3f482)`(unsigned int numBreakPoints,unsigned int minFrequency,unsigned int maxFrequency,unsigned int bufferSize)` 

Initialize the class with required defaults.

#### `public  `[`~Gendy`](#classGendy_1a4317e1fc53d407c165eb626cd48e3d89)`()` 

#### `public void `[`setup`](#classGendy_1a9f8c0e7adfc17e0d7fa9844531f65e70)`(unsigned int numBreakPoints,unsigned int minFrequency,unsigned int maxFrequency,unsigned int bufferSize)` 

Initialize the class with required defaults Either Initialize at construction or use an empty constructor and call `[Gendy::setup](#classGendy_1a9f8c0e7adfc17e0d7fa9844531f65e70)` at a later stage

#### `public double `[`drunkWalk`](#classGendy_1aaf5753120e1374ece4bb29352307d8b7)`()` 

A simple implementation of Brownian motion. This call returns a single value. Use it in a `while` loop for continuous updating

#### `public double `[`poission`](#classGendy_1a537ab206639e1601ab1e9d0df3982198)`()` 

A simple implementation of Poission distribution This call returns a single value. Use it in a `while` loop for continuous updates

#### `public void `[`process`](#classGendy_1a0b16221aaa81a6e4a3e109cd6fb0f67d)`()` 

The implementation of [Gendy](#classGendy) is multithreaded. This call initiates the process.

#### `public void `[`threadedFunction`](#classGendy_1ad98694e34171a373dfbdd254c34f73c6)`()` 

This is the threaded function that executes the [Gendy](#classGendy) algorithm. This generates an array of breakpoints using the desired random distribution function. Since it is a threaded function, while this process is ongoing, the array cannot be accessed by other functions.

#### `public double `[`getGendySample`](#classGendy_1a42580a4bbb474612012585f06375b633)`()` 

Calculates and returns a single sample from the [Gendy](#classGendy) waveform buffer. It updates the counter for each call to use as an index inside the array. When the counter reaches the end of the array, it restarts `[Gendy::process](#classGendy_1a0b16221aaa81a6e4a3e109cd6fb0f67d)` to calculate new breakpoints and repeats its internal functions.

#### `public void `[`setFrequencies`](#classGendy_1a8230d0b38c7a12e4dae31745a577d7e1)`(float minFreq,float maxFreq)` 

Set the bounds of minimum and maximum frequency values for breakpoint bounds The normalized values `minFreq/numBreakpoints` set the new bounds. This restarts the [Gendy](#classGendy) process using `[Gendy::process](#classGendy_1a0b16221aaa81a6e4a3e109cd6fb0f67d)`

#### `public void `[`setNewBreakpoints`](#classGendy_1a6656e6af1bfe340f67ad74fdf8b99a8b)`(int maxLength,int numBreakPoints)` 

Set new values for number of breakpoints and max size of the array This restarts the [Gendy](#classGendy) process using `[Gendy::process](#classGendy_1a0b16221aaa81a6e4a3e109cd6fb0f67d)`

#### `public void `[`plotGendy`](#classGendy_1afdb82a587cbc9a9f77a106be9e12f958)`()` 

Plot the [Gendy](#classGendy) waveform using `gnuplot`

#### `public inline double `[`getGendyAt`](#classGendy_1afa7001a6d8663361cf7192e3fc0ad406)`(unsigned int position)` 

Returns a single [Gendy](#classGendy) waveform sample at the requested index.

# class `ofApp` 

```
class ofApp
  : public ofBaseApp
```  

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public ofSoundStream `[`soundStream`](#classofApp_1a32f542fc195492272e0bb8bd4858c4fe) | The function responsible for threaded handling of `[ofApp::audioOut](#classofApp_1aae87e3581084ad5b4fa5584f0871092e)`
`public int `[`sampleRate`](#classofApp_1abd83953a5dcff8f0deabbfc2a0170d98) | 
`public int `[`bufferSize`](#classofApp_1af573fde175597807abfefd7b1c34c457) | 
`public int `[`numBreakPoints`](#classofApp_1a9d1b0ae68eabe8d914ea7f5b43b244f8) | 
`public double `[`currentSample`](#classofApp_1a0539db6c43d9ddaae6aa82f79fd6ef60) | 
`public int `[`minFrequency`](#classofApp_1a4f34f2c284917c6c7f31bce47b3ab8e2) | 
`public int `[`maxFrequency`](#classofApp_1abc325066a075e9e973b4d9523d01a25c) | 
`public int `[`currentGendyPosition`](#classofApp_1a102a9c9bb153c42f2272bec5650d214d) | 
`public int `[`maxLength`](#classofApp_1ae7f08127759ea2153a87f10f479ac985) | 
`public `[`Gendy`](#classGendy)` `[`gendy`](#classofApp_1a07cf87e026b35f0f764fe9cf2adb9171) | Initiate [Gendy](#classGendy) class.
`public ofSoundBuffer `[`viewBuffer`](#classofApp_1a6864328bd68685ed3cc7fd5c86f82e5d) | A vector to store [Gendy](#classGendy) waveform to be drawn on screen.
`public std::mutex `[`audioMutex`](#classofApp_1a12c9d82caf0e256edb9c9aae1547a672) | A mutex to lock the [Gendy](#classGendy) buffer when it is being used to create visual waveform.
`public ofPolyline `[`waveForm`](#classofApp_1a78391a5b16abc2ead09c474278e45fcd) | An `openFrameworks` class to pass vertex data to `openGL`
`public float `[`rms`](#classofApp_1ac3cbc470889b7177ff48a4a20c89101b) | 
`public float `[`amplitude`](#classofApp_1ae8d93c6598d7959ecaa1d30ec07f41ec) | 
`public int `[`position`](#classofApp_1ae45307c752842a0f5ef84529a071032e) | 
`public int `[`posIndex`](#classofApp_1a99cc6a802dae5a8a0f0ca8dd742360f9) | 
`public void `[`setup`](#classofApp_1af68eaa1366244f7a541cd08e02199c12)`()` | 
`public void `[`update`](#classofApp_1afef41ea4aee5a22ea530afba33ae7a7b)`()` | Clear the waveform each frame and get new values into the `[ofApp::waveForm](#classofApp_1a78391a5b16abc2ead09c474278e45fcd)` Calculate RMS to set visual bounds
`public void `[`draw`](#classofApp_1a75dd45437b9e317db73d8daef1ad49f8)`()` | Draw [Gendy](#classGendy) waveform each frame.
`public void `[`audioOut`](#classofApp_1aae87e3581084ad5b4fa5584f0871092e)`(ofSoundBuffer & buffer)` | A threaded function that opens communication with audio hardware using `RTaudio` api This function is called recursively once every samplerate/buffersize Upon each call, it executes the code within the function that is responsible for creating and storing audio buffer Multichannel audio is simply interleaved
`public void `[`exit`](#classofApp_1a41588341bbe9be134f6abdc2eb7cfd4c)`()` | Close the sound stream communication on exit.
`public void `[`keyPressed`](#classofApp_1a957d3197364bbac8e67eaa4f15b28ad3)`(int key)` | Plot the [Gendy](#classGendy) wave when pressing "s" key.
`public void `[`mousePressed`](#classofApp_1a2c2ea9c160231e55424dfd98466ef27d)`(int x,int y,int button)` | Use Left mouse button to set new frequencies using standard random engine Use Right mouse button to set start a new breakpoint buffer using standard random distribution engine

## Members

#### `public ofSoundStream `[`soundStream`](#classofApp_1a32f542fc195492272e0bb8bd4858c4fe) 

The function responsible for threaded handling of `[ofApp::audioOut](#classofApp_1aae87e3581084ad5b4fa5584f0871092e)`

#### `public int `[`sampleRate`](#classofApp_1abd83953a5dcff8f0deabbfc2a0170d98) 

#### `public int `[`bufferSize`](#classofApp_1af573fde175597807abfefd7b1c34c457) 

#### `public int `[`numBreakPoints`](#classofApp_1a9d1b0ae68eabe8d914ea7f5b43b244f8) 

#### `public double `[`currentSample`](#classofApp_1a0539db6c43d9ddaae6aa82f79fd6ef60) 

#### `public int `[`minFrequency`](#classofApp_1a4f34f2c284917c6c7f31bce47b3ab8e2) 

#### `public int `[`maxFrequency`](#classofApp_1abc325066a075e9e973b4d9523d01a25c) 

#### `public int `[`currentGendyPosition`](#classofApp_1a102a9c9bb153c42f2272bec5650d214d) 

#### `public int `[`maxLength`](#classofApp_1ae7f08127759ea2153a87f10f479ac985) 

#### `public `[`Gendy`](#classGendy)` `[`gendy`](#classofApp_1a07cf87e026b35f0f764fe9cf2adb9171) 

Initiate [Gendy](#classGendy) class.

#### `public ofSoundBuffer `[`viewBuffer`](#classofApp_1a6864328bd68685ed3cc7fd5c86f82e5d) 

A vector to store [Gendy](#classGendy) waveform to be drawn on screen.

#### `public std::mutex `[`audioMutex`](#classofApp_1a12c9d82caf0e256edb9c9aae1547a672) 

A mutex to lock the [Gendy](#classGendy) buffer when it is being used to create visual waveform.

#### `public ofPolyline `[`waveForm`](#classofApp_1a78391a5b16abc2ead09c474278e45fcd) 

An `openFrameworks` class to pass vertex data to `openGL`

#### `public float `[`rms`](#classofApp_1ac3cbc470889b7177ff48a4a20c89101b) 

#### `public float `[`amplitude`](#classofApp_1ae8d93c6598d7959ecaa1d30ec07f41ec) 

#### `public int `[`position`](#classofApp_1ae45307c752842a0f5ef84529a071032e) 

#### `public int `[`posIndex`](#classofApp_1a99cc6a802dae5a8a0f0ca8dd742360f9) 

#### `public void `[`setup`](#classofApp_1af68eaa1366244f7a541cd08e02199c12)`()` 

#### `public void `[`update`](#classofApp_1afef41ea4aee5a22ea530afba33ae7a7b)`()` 

Clear the waveform each frame and get new values into the `[ofApp::waveForm](#classofApp_1a78391a5b16abc2ead09c474278e45fcd)` Calculate RMS to set visual bounds

#### `public void `[`draw`](#classofApp_1a75dd45437b9e317db73d8daef1ad49f8)`()` 

Draw [Gendy](#classGendy) waveform each frame.

#### `public void `[`audioOut`](#classofApp_1aae87e3581084ad5b4fa5584f0871092e)`(ofSoundBuffer & buffer)` 

A threaded function that opens communication with audio hardware using `RTaudio` api This function is called recursively once every samplerate/buffersize Upon each call, it executes the code within the function that is responsible for creating and storing audio buffer Multichannel audio is simply interleaved

#### `public void `[`exit`](#classofApp_1a41588341bbe9be134f6abdc2eb7cfd4c)`()` 

Close the sound stream communication on exit.

#### `public void `[`keyPressed`](#classofApp_1a957d3197364bbac8e67eaa4f15b28ad3)`(int key)` 

Plot the [Gendy](#classGendy) wave when pressing "s" key.

#### `public void `[`mousePressed`](#classofApp_1a2c2ea9c160231e55424dfd98466ef27d)`(int x,int y,int button)` 

Use Left mouse button to set new frequencies using standard random engine Use Right mouse button to set start a new breakpoint buffer using standard random distribution engine

Generated by [Moxygen](https://sourcey.com/moxygen)