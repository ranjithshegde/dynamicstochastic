#pragma once

#include "Gendy.h"
#include "ofMain.h"

class ofApp : public ofBaseApp {

public:
    void setup();
    void update();
    void draw();
    //! A threaded function that opens communication with audio hardware using `RTaudio` api
    //! This function is called recursively once every samplerate/buffersize
    //! Upon each call, it executes the code within the function that is responsible for creating and storing audio buffer
    //! Multichannel audio is simply interleaved
    void audioOut(ofSoundBuffer& buffer);
    void exit();

    void keyPressed(int key);
    void mousePressed(int x, int y, int button);

    //! The function responsible for threaded handling of `ofApp::audioOut`
    ofSoundStream soundStream;
    int sampleRate;
    int bufferSize;

    int numBreakPoints;
    double currentSample;
    int minFrequency, maxFrequency;
    int currentGendyPosition;
    int maxLength;

    //! Initiate Gendy class
    Gendy gendy;
    //! A vector to store Gendy waveform to be drawn on screen
    ofSoundBuffer viewBuffer;
    //! A mutex to lock the Gendy buffer when it is being used to create visual waveform
    std::mutex audioMutex;
    //! An `openFrameworks` class to pass vertex data to `openGL`
    ofPolyline waveForm;
    float rms;
    float amplitude;
    int position, posIndex;
};
