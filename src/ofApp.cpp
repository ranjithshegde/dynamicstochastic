#include "ofApp.h"
#include "ofMath.h"
//--------------------------------------------------------------
void ofApp::setup()
{
    sampleRate = 48000;
    bufferSize = 1024;

    ofSoundStreamSettings settings;
    settings.setOutListener(ofGetAppPtr());
    settings.sampleRate = sampleRate;
    settings.bufferSize = bufferSize;
    settings.numOutputChannels = 2;
    settings.numBuffers = 2;
    soundStream.setup(settings);
    maxLength = 512;

    numBreakPoints = 30;
    currentSample = 0;
    currentGendyPosition = 0;

    minFrequency = 10;
    maxFrequency = 60;

    gendy.setup(numBreakPoints, minFrequency, maxFrequency, maxLength);

    amplitude = 0.5;
    gendy.process();
    position = 0;
    posIndex = 0;
}

//--------------------------------------------------------------
//! Clear the waveform each frame and get new values into the `ofApp::waveForm`
//! Calculate RMS to set visual bounds
void ofApp::update()
{
    unique_lock<mutex> lock(audioMutex);
    waveForm.clear();
    for (size_t i = 0; i < viewBuffer.getNumFrames(); i++) {
        double sample = viewBuffer.getSample(i, 0);
        double x = ofMap(i, 0, viewBuffer.getNumFrames(), 0, ofGetWidth());
        double y = ofMap(sample, -1.0, 1.0, 0, ofGetHeight());
        waveForm.addVertex(x, y);
    }
    rms = viewBuffer.getRMSAmplitude();
}

//--------------------------------------------------------------
//! Draw Gendy waveform each frame
void ofApp::draw()
{
    ofBackground(ofColor::black);
    ofSetColor(ofColor::white);
    ofSetLineWidth(1 + (rms * 30.0));
    waveForm.draw();
}

//--------------------------------------------------------------
void ofApp::audioOut(ofSoundBuffer& buffer)
{
    for (size_t i = 0; i < buffer.getNumFrames(); i++) {

        currentSample = gendy.getGendySample() * amplitude;
        buffer[i * buffer.getNumChannels()] = currentSample;
        buffer[i * buffer.getNumChannels() + 1] = currentSample;
    }
    unique_lock<mutex> lock(audioMutex);
    viewBuffer = buffer;
}

//--------------------------------------------------------------
//! Plot the Gendy wave when pressing "s" key
void ofApp::keyPressed(int key)
{
    if (key == 's') {
        gendy.plotGendy();
    }
}

//--------------------------------------------------------------
//! Use Left mouse button to set new frequencies using standard random engine
//! Use Right mouse button to set start a new breakpoint buffer using standard random distribution engine
void ofApp::mousePressed(int x, int y, int button)
{
    if (button == OF_MOUSE_BUTTON_3) {
        int newMax = ofRandom(200, 700);
        int newSize = ofRandom(12, 40);
        gendy.setNewBreakpoints(newMax, newSize);
    }
    if (button == OF_MOUSE_BUTTON_1) {
        minFrequency = (int)ofRandom(5, 30);
        maxFrequency = (int)ofRandom(50, 100);
        gendy.setFrequencies(minFrequency, maxFrequency);
    }
}

//--------------------------------------------------------------
//! Close the sound stream communication on exit
void ofApp::exit()
{
    soundStream.close();
    // gendy.plotGendy();
}
