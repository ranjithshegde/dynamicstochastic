#include "Gendy.h"

Gendy::Gendy()
{
}

Gendy::Gendy(unsigned int numBreakPoints, unsigned int minFrequency, unsigned int maxFrequency, unsigned int bufferSize)
    : m_NumBreakPoints(numBreakPoints)
    , m_MaxLength(bufferSize)
    , m_Count(0)
    , m_IsProcessed(false)
    , m_ToReset(false)
    , m_LastValue(0.f)
{
    m_BreakPoints.resize(m_NumBreakPoints + 1);
    m_GendyBuffer.resize(m_MaxLength);
    m_MinSample = minFrequency / m_NumBreakPoints;
    m_MaxSample = maxFrequency / m_NumBreakPoints;
    p_outfile.open("data.txt");
}

void Gendy::setup(unsigned int numBreakPoints, unsigned int minFrequency, unsigned int maxFrequency, unsigned int bufferSize)
{
    m_ToReset = false;

    m_NumBreakPoints = numBreakPoints;
    m_MaxLength = bufferSize;
    m_Count = 0;
    m_LastValue = 0.f;
    m_BreakPoints.resize(m_NumBreakPoints + 1);
    m_GendyBuffer.resize(m_MaxLength);
    m_MinSample = minFrequency / m_NumBreakPoints;
    m_MaxSample = maxFrequency / m_NumBreakPoints;

    m_IsProcessed = false;
}

double Gendy::drunkWalk()
{
    double value = ofRandom(1.0f);
    int orientation = (int)ofRandom(10);
    if (orientation % 2 == 0) {
        value *= -1;
    }
    return value;
}

double Gendy::poission()
{
    unsigned seed = std::chrono::steady_clock::now().time_since_epoch().count();
    std::default_random_engine e(seed);
    std::normal_distribution<double> pois(1.0);

    double value = pois(e);

    if ((int)ofRandom(10) % 2 == 0) {
        value *= -1;
    }
    return value;
}

void Gendy::process()
{
    if (m_ToReset) {
        m_ResetBuffers();
    }
    startThread();
}

void Gendy::threadedFunction()
{
    lock();
    m_BreakPoints[0] = 0;
    m_GendyBuffer[m_BreakPoints[0]] = m_LastValue;

    for (unsigned int i = 1; i < m_NumBreakPoints; i++) {
        m_BreakPoints[i] = ofClamp(m_BreakPoints[i - 1] + (int)ofRandom(m_MinSample, m_MaxSample), 0, m_MaxLength);
        m_GendyBuffer[m_BreakPoints[i]] = drunkWalk();
        // m_GendyBuffer[m_BreakPoints[i]] = poission();
    }
    m_BreakPoints[m_NumBreakPoints] = m_MaxLength;
    // m_GendyBuffer[m_BreakPoints[m_NumBreakPoints]] = poission();
    m_GendyBuffer[m_BreakPoints[m_NumBreakPoints]] = drunkWalk();
    m_LastValue = m_GendyBuffer[m_BreakPoints[m_NumBreakPoints]];

    for (unsigned int i = 0; i < m_NumBreakPoints; i++) {
        // cout << "The breakpoint at i = " << i << " : " << m_BreakPoints[i] << endl;
        // cout << "The last breakpoint is " << m_BreakPoints[m_NumBreakPoints] << endl;
        for (unsigned int j = m_BreakPoints[i] + 1; j < m_BreakPoints[i + 1]; ++j) {
            float normal = j / (float)m_BreakPoints[i + 1];
            // m_GendyBuffer[j] = normal * m_GendyBuffer[m_BreakPoints[i]] + (1 - normal) * m_GendyBuffer[m_BreakPoints[i + 1]];
            m_GendyBuffer[j] = ofLerp(m_GendyBuffer[m_BreakPoints[i]], m_GendyBuffer[m_BreakPoints[i + 1]], normal);
            // cout << "linear interpolating between " << m_GendyBuffer[m_BreakPoints[i]] << " and " << m_GendyBuffer[m_BreakPoints[i + 1]] << endl;
            // cout << "The value at J : " << j << " is = " << m_GendyBuffer[j] << endl;
        }
    }
    unlock();
    m_IsProcessed = true;
}

double Gendy::getGendySample()
{
    if (!m_IsProcessed) {
        process();
    }

    lock();
    double value = m_GendyBuffer[m_Count];
    unlock();

    m_Count++;

    if (m_Count == 0 || m_Count == m_MaxLength) {
        process();
        m_Count = 0;
    }
    return value;
}

void Gendy::setNewBreakpoints(int maxLength, int numBreakPoints)
{
    m_MaxLength = maxLength;
    m_NumBreakPoints = numBreakPoints;
    m_ToReset = true;
    process();
}

void Gendy::setFrequencies(float minFreq, float maxFreq)
{
    m_MinSample = minFreq / m_NumBreakPoints;
    m_MaxSample = maxFreq / m_NumBreakPoints;
    m_ToReset = true;
    process();
}

// added becuse of repeated segfaults. should not be needed;
void Gendy::m_ResetBuffers()
{
    lock();
    m_BreakPoints.clear();
    m_GendyBuffer.clear();
    m_BreakPoints.resize(m_NumBreakPoints + 1);
    m_GendyBuffer.resize(m_MaxLength);
    unlock();
    m_ToReset = false;
}

void Gendy::plotGendy()
{
    lock();
    for (unsigned i = 0; i < m_GendyBuffer.size(); i++) {
        p_outfile << m_GendyBuffer[i] << endl;
    }
    unlock();
    system("gnuplot -p -e \"plot 'data.txt' with lines\"");
}

Gendy::~Gendy()
{
    p_outfile.close();
}
