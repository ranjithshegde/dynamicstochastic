#pragma once
#include "ofMain.h"
#include "random"

class Gendy : public ofThread {
public:
    //! This is the Gendy algorithm as defined by Iannis Xenakis and implemented by Sergio Luque
    //! Breakpoints(x for time and y for amplitude) are generated using certain stochastic methods.
    //! Interpolate between the breakpoints to generate a new waveform, either for playback as audio or for control values
    Gendy();

    //! Initialize the class with required defaults
    Gendy(unsigned int numBreakPoints, unsigned int minFrequency, unsigned int maxFrequency, unsigned int bufferSize);

    ~Gendy();

    //! Initialize the class with required defaults
    //! Either Initialize at construction or use an empty constructor and call `Gendy::setup` at a later stage
    void setup(unsigned int numBreakPoints, unsigned int minFrequency, unsigned int maxFrequency, unsigned int bufferSize);

    //! A simple implementation of Brownian motion.
    //! This call returns a single value. Use it in a `while` loop for continuous updating
    double drunkWalk();

    //! A simple implementation of Poission distribution
    //! This call returns a single value. Use it in a `while` loop for continuous updates
    double poission();

    //! The implementation of Gendy is multithreaded. This call initiates the process
    void process();

    //! This is the threaded function that executes the Gendy algorithm.
    //! This generates an array of breakpoints using the desired random distribution function.
    //! Since it is a threaded function, while this process is ongoing, the array cannot be accessed by other functions.
    void threadedFunction();

    //! Calculates and returns a single sample from the Gendy waveform buffer.
    //! It updates the counter for each call to use as an index inside the array.
    //! When the counter reaches the end of the array, it restarts `Gendy::process` to calculate new breakpoints and repeats its internal functions.
    double getGendySample();

    //! Set the bounds of minimum and maximum frequency values for breakpoint bounds
    //! The normalized values `minFreq/numBreakpoints` set the new bounds.
    //! This restarts the Gendy process using `Gendy::process`
    void setFrequencies(float minFreq, float maxFreq);

    //! Set new values for number of breakpoints and max size of the array
    //! This restarts the Gendy process using `Gendy::process`
    void setNewBreakpoints(int maxLength, int numBreakPoints);

    //! Plot the Gendy waveform using `gnuplot`
    void plotGendy();

    //! File stream (& descriptor) to store Gendy pcm values
    ofstream p_outfile;

    //! Returns a single Gendy waveform sample at the requested index
    inline double getGendyAt(unsigned int position)
    {
        lock();
        double pos = m_GendyBuffer[position];
        unlock();
        return pos;
    }

private:
    unsigned int m_NumBreakPoints, m_MaxLength;
    unsigned int m_MinSample, m_MaxSample;
    unsigned int m_Count;
    bool m_IsProcessed;
    bool m_ToReset;
    double m_LastValue;

    std::vector<double> m_GendyBuffer;
    std::vector<int> m_BreakPoints;
    std::vector<double> m_history;

    void m_ResetBuffers();
};
